// src/index.ts

import Vue from "vue";
import HelloComponent from "./components/Hello.vue";

import { SVG } from '@svgdotjs/svg.js';
import "@svgdotjs/svg.draggable.js";

let v = new Vue({
    el: "#app",
    template: `
    <div>
	<hello-component />
    </div>`,
    data: {
        name: "World"
    },
    components: {
	    HelloComponent
	}
});
